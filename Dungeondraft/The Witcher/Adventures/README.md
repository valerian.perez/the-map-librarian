# La campagne

*Adventures - Enhanced Edition* est un receuil d'aventure disponible [ici](https://www.the-witcher-jdr.fr/campagnes.php) qui propose 11 Scénarios gratuits distribués par [Copernicus Corporation](https://copcorp.pl/), le distributeur polonais du jeu de rôle The Witcher et traduits et amélioré par la communauté. 

## Le contenu

### La Montagne de Verre

*Les joueurs participent à une expédition au château de Gwydberg, siégeant dans la vallée de Roen dans les montagnes marquant la frontière entre Užupis et Angren.
Cette forteresse abandonnée il y a près de deux décennies, est supposé abriter un trésor enfoui, c'est pour cela que les PJ devront investiguer pour mériter leur prime. 
Une fois sur place, ils découvriront qu'ils sont en compétition avec un autre groupe. 
Le château lui-même cache plus d'un secret. 
Tout d'abord, ils devront décider du sort des deux héritières du château, affronter la malédiction qui y règne et peut-être exploiter le pouvoir d'un puissant arbre maudit au centre du château. 
Si les PJ réussissent, ils gagneront bien plus que l'argent promis. 
Ils seront capables de bâtir une nouvelle maison. Si tout tourne mal, ils acquerront un puissant ennemi surnaturel. 
Enfin… s’ils survivent.*

Le scénario de la montagne de verre peut être un bon début de campagne pour le jeu de rôle : The Witcher, car il est un moyen de lier les membres d’une équipe. 
Il est écrit pour un groupe de 3 à 6 personnages débutants et devrait durer entre 8 et 15 heures de jeu (pouvant se découper entre 2 et 3 sessions standards). 
A la fin du scénario, les personnages peuvent avoir une chance de posséder leur propre demeure, ou du moins un point de départ pour d'autres aventures dans la région.

### Le don du misérable

### Immunisé - La peste de Vizima

### Rencontres sur la route (Série de mini-scénarios)

### C'est arrivé à Belleteyn

### La bête d'Urialla

### L'or noir

### Aucune demande d'aide n'est refusée sur la route

### Un village heureux est un village mort

### La vierge var Attre

### Les fonatômes de Trocken
