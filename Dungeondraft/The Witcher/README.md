## Le jeu

[The Witcher, le jeu de rôle officiel](https://www.arkhane-asylum.fr/the-witcher/) est un jeu de rôle édité en France par [Arkhane Asylum](https://www.arkhane-asylum.fr/).

## L'univers

En pleine Troisième Guerre nilfgaardienne, Geralt de Riv, le Loup Blanc, écume le Continent à la recherche de son amour perdu. Mais il ne s’agit pas du seul récit. Un million d’autres aventures se jouent au travers de ce vaste continent et vous êtes au centre de l’une d’entre elles !

Le jeu de rôle The Witcher vous permet de raconter vos propres histoires dans le légendaire monde du Sorceleur. Aventurez-vous sur le Continent, interagissez avec des légendes vivantes comme Yennefer de Vengerberg ou Vernon Roche, et influencez les politiques locales. Choisissez votre camp et combattez dans la brutale Troisième Guerre nilfgaardienne, ou jouez vos propres aventures et couvrez-vous de gloire !
